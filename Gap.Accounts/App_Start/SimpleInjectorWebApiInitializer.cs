using Gap.Accounts.Services;

[assembly: WebActivator.PostApplicationStartMethod(typeof(Gap.Accounts.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace Gap.Accounts.App_Start
{
    using System.Web.Http;
    using ErpCommunicator;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using SimpleInjector.Lifestyles;
    
    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            
            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {
            container.Register<IDepotService,DepotService>();
            container.Register<IFinanceSystemFinder, FinanceSystemFinderService>();
            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
            var fac = ErpFactory.Build();
            container.Register<IErpFactory>(() => fac);

        }
    }
}