﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.XPath;
using Microsoft.Owin;
using Owin;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(Gap.Accounts.Startup))]

namespace Gap.Accounts
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "MyAPI");
                    c.IncludeXmlComments(GetXmlCommentsPath());
                })
                .EnableSwaggerUi();
        }

        protected static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\bin\MyAPI.XML", System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
