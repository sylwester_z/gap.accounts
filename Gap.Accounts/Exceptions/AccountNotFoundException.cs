﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gap.Accounts.Exceptions
{
    public class AccountNotFoundException : Exception
    {
        public AccountNotFoundException(string message)
            : base(message)
        {
        }

        public AccountNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}