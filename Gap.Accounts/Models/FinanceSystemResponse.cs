﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ErpCommunicator.Accounts;

namespace Gap.Accounts.Models
{
    public class FinanceSystemResponse
    {
        public Account Account { get; set; }
        public FinanceSystem FinanceSystem { get; set; }

        public Exception Exception { get; set; }

        public bool Sucess => Exception == null;
    }

    public enum FinanceSystem
    {
        Dimensions = 1,
        Sage = 3
    }
}