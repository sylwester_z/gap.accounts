﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gap.Accounts.Data;

namespace Gap.Accounts.Models
{
    public class DepotDto
    {
        public Depot Depot { get; set; }
        public DepotIP DepotIp { get; set; }
    }
}