﻿using System.Web.Http;
using Gap.Accounts.Services;
using WebApi.OutputCache.V2;

namespace Gap.Accounts.Controllers.Api
{
    [RoutePrefix("api/financesystem")]
    public class FinanceSystemController : ApiController
    {
        private readonly IFinanceSystemFinder _accountService;

        public FinanceSystemController(IFinanceSystemFinder accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Get Finance system for Account
        /// </summary>
        /// <param name="accountNo">Sage Trade Account Number</param>
        /// <returns></returns>
        [CacheOutput(ClientTimeSpan = 9600, ServerTimeSpan = 9600)]
        [Route("get/{accountNo}")]
        public IHttpActionResult Get(string accountNo)
        {
            var result = _accountService.GetFinanceSystemId(accountNo);
            return Ok(result);
        }
    }
}
