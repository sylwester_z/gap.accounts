﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ErpCommunicator;
using ErpCommunicator.Accounts;
using Gap.Accounts.Exceptions;
using Gap.Accounts.Models;

namespace Gap.Accounts.Services
{
    public interface IFinanceSystemFinder
    {
        FinanceSystemResponse GetFinanceSystemId(string accountNo);
    }

    public class FinanceSystemFinderService : IFinanceSystemFinder
    {
        private readonly IGetAccountDetails _accountDetails;
        private readonly IDepotService _depotService;

        public FinanceSystemFinderService(IErpFactory factory, IDepotService depotService)
        {
            _depotService = depotService;
            _accountDetails = factory.Instance<IGetAccountDetails>();
        }

        public FinanceSystemResponse GetFinanceSystemId(string accountNo)
        {
            FinanceSystemResponse response = new FinanceSystemResponse();
            try
            {
                response.Account = _accountDetails.GetAccountDetails(accountNo);
                DepotDto depot = GetDepotForAccount(response.Account);
                response.FinanceSystem = (FinanceSystem)depot.DepotIp.AccountSystem;
            }
            catch (Exception e)
            {
                response.Exception = e;
            }

            return response;
        }

        private DepotDto GetDepotForAccount(Account account)
        {

            CheckIfAccountHasAssignetDepot(account);

            var depot = _depotService.GetDepotBySageCode(account.StatGroups[StatId.CustomerStat1].Id);
            return depot;


        }

        private void CheckIfAccountHasAssignetDepot(Account account)
        {
            if (string.IsNullOrEmpty(account.AccountCode))
            {
                throw new AccountNotFoundException($"Account doesn't exist");
            }
            if (string.IsNullOrEmpty(account.StatGroups?[StatId.CustomerStat1].Value))
            {
                throw new AccountNotFoundException($"Account hasn't got setup customer stat 1");
            }

        }
    }
}