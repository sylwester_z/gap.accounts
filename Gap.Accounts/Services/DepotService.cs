﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gap.Accounts.Data;
using Gap.Accounts.Models;

namespace Gap.Accounts.Services
{
    public interface IDepotService
    {
        DepotDto GetDepotBySageCode(string sageDepotNo);
    }

    public class DepotService : IDepotService
    {
        private readonly CoreEntities _db;

        public DepotService()
        {
            _db = new CoreEntities();
        }

        public DepotDto GetDepotBySageCode(string sageDepotNo)
        {
            var result = (from depot in _db.Depots
                
                let depotip = _db.DepotIPs.FirstOrDefault(f => f.Depot == depot.DepotNumber)

                select new DepotDto()
                {
                    Depot = depot,
                    DepotIp = depotip
                }).FirstOrDefault(f => f.Depot.SageSite == sageDepotNo);

            return result;

        }
    }
}